![N|Solid](https://s1.postimg.org/16403szunz/favicon-96x96.png)
# Hackamonth: Lunch

The Lunch application is a simple web application hacked together as a part of the Aller Finland Hackamonth in October of 2017.

##### Features:

  - Simple UI to generate a random restaurant to visit for lunch
  - Mobile friendly, 100% responsive

##### Installation:

Clone the GIT repository like this:

```sh
cd /the/location/of/your/choice
git clone https://thomasdjupsjo@bitbucket.org/allermedia/fi-hackmonth-lunch.git
```

##### Next steps:

 - Share the app to the employees
 - Integrate Lounaat.info API or similar for dynamic content

##### License & Author
Aller Media Oy &copy;
Thomas Djupsjö